.TH "IMPORT_TO_BLENDER" "1" "2023-11-05" "Geek Central" "Batch-Mode Blender Scripts"
.
.SH NAME
import_to_blender \(em invoke
.UR https://blender.org/
Blender
.UE
importer addons in batch mode, with automatic expansion of archive files
.
.SH SYNOPSIS
\fBimport_to_blender\fR [\fIoptions\fR ...] \fIinput_file\fR \fIoutput_blend_file\fR
.
.SH DESCRIPTION
.B import_to_blender
is a command-line tool which, given an input file, will launch Blender
in batch mode and invoke its available importer addons to convert
the file to
.B .blend
format. The extension on
.I input_file
will be used to find a recognized importer, or it can be one of
the common archive formats, in which case it will be extracted
to a temporary directory and its contents used to find importers.
.
The contents of each imported file will be put into a separate
collection in the destination
.B .blend
file.
.
Any external image textures that are referenced (for example,
included in the archive file) will be packed into the
.B .blend
file, making it self-contained. Their associated file paths will
be set to something reasonable, allowing them to be conveniently
extracted if needed.
.
.I output_blend_file
can also be a directory name, in which case the output
.B .blend
file will be created within it, with the same base name as
.IR input_file .
.
.SH OPTIONS
.TP
\fB\-\-arg=\fIimpname\fB:\fIkeyword\fB=\fIvalue\fR
specifies an additional keyword argument for the import operator
for the format \fIimpname\fR. May be specified multiple times
with different keywords for different importers.
.
.TP
\fB\-\-blender=\fIblender\fR
specifies the path to the Blender executable. Defaults to
searching for the name “blender” in your \fBPATH\fR.
.
.TP
\fB\-\-existing=error\fR|\fBmerge\fR|\fBoverwrite\fR
specifies what to do if the output .blend file already exists:
.RS
.TP
.B error
\(em signal an error and abort the conversion (default)
.
.TP
.B merge
\(em merge the imported models into the existing
.B .blend
file.
.
.TP
.B overwrite
\(em overwrite the output
.B .blend
file
.RE
.
.TP
.B \-\-quiet
Reduce messages displayed.
.
.TP
\fB\-\-postexec=\fIscript\fR
executes the specified Python code after the import, just prior
to saving the
.B .blend
file. May be specified multiple times; the values are concatenated
in sequence, separated by newlines. May not be specified
together with
.BR \-\-postexecfile .
.
See \fBUSER CODE EXECUTION CONTEXT\fR below for an explanation of
the context in which \fIscript\fR is executed.
.
.TP
\fB\-\-postexecfile=\fIscriptfilename\fR
executes the contents of the specified text file as Python code
after the import, just prior to saving the .blend file.
May not be specified together with
.BR \-\-postexec .
.
See \fBUSER CODE EXECUTION CONTEXT\fR below for an explanation of
the context in which the contents of \fIscriptfilename\fR is executed.
.
.TP
\fB\-\-preexec=\fIscript\fR
executes the specified Python code prior to the import.
May be specified multiple times; the values are concatenated
in sequence, separated by newlines. May not be specified
together with
.BR \-\-preexecfile .
.
See \fBUSER CODE EXECUTION CONTEXT\fR below for an explanation of
the context in which \fIscript\fR is executed.
.
.TP
\fB\-\-preexecfile=\fIscriptfilename\fR
executes the contents of the specified text file as Python code
prior to the import. May not be specified together with
.BR \-\-preexec .
.
See \fBUSER CODE EXECUTION CONTEXT\fR below for an explanation of
the context in which the contents of \fIscriptfilename\fR is executed.
.
.TP
\fB\-\-template=\fItemplate-name\fR
specifies the startup template to load before doing the import.
.
.TP
.B \-\-test
indicates just to test the input file for importability,
without doing the actual import. Returns a status of 0 if it
looks importable, 1 if not.
.
.SH USER CODE EXECUTION CONTEXT
.
To avoid clashes with the namespace of the \fBimport_to_blender\fR
program itself, your script code that is specified via \fB\-\-preexec\fR,
\fB\-\-preexecfile\fR, \fB\-\-postexec\fR or \fB\-\-postexecfile\fR
is executed within the global context of a special module called
\fBuserdefs\fR. Initially this module is empty apart from an automatic
import of Blender’s \fBbpy\fR Python API module; any globals you define
will be kept here for use in subsequent exec calls.
.
.SH RECOGNIZED IMPORTERS
.
.B import_to_blender
recognizes the standard Blender importers for Alembic
.RB ( .abc ),
Collada
.RB ( .dae ),
FBX, glTF
.RB ( .gltf / .glb ),
OBJ, PLY, STL and X3D file formats. It will also import 3DS, LWO
(LightWave objects) and XPS (“XNALara”) formats, provided it
can find the requisite importer addons.
.
.B import_to_blender
can even handle an archive file that already contains a native
.B .blend
file, extracting that and packing in its dependencies from the archive.
.
.SH EXAMPLES
.
.PP
.RS 4
\fBimport_to_blender model.obj .\fR
.RE
.
.PP
Imports
.B model.obj
as an OBJ-format model, creating the file
.B model.blend
in the current directory.
.
.PP
.RS 4
\fBimport_to_blender archive.zip converted.blend\fR
.RE
.
.PP
Extracts
.B archive.zip
to a temporary directory, and invokes an appropriate importer
on the contents, saving the result as the file
.BR converted.blend .
.
.SH EXTERNAL PROGRAMS NEEDED
.
.B import_to_blender
relies on
.BR unar (1)
to extract archive formats.
.
.SH SEE ALSO
.
.BR unar (1)
